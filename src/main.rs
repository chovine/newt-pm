use clap::{Arg, App, SubCommand};
use std::process::Command;
use std::fs;
use std::path::{PathBuf,Path};
use std::io::BufReader;
use std::io::prelude::*;
use regex::Regex;
use std::collections::HashSet;

const PKG_DIR: &str = "/var/newt/pkgs";
const INSTALLED_PKG: &str = "/var/newt/installed";

fn main() {
    let matches = App::new("Newt Package Manager")
                          .version("0.1")
                          .author("Charles H. <charles@hovine.io>")
                          .about("A package manager for linux+gnu")
                          .arg(Arg::with_name("root")
                               .short("r")
                               .long("root")
                               .value_name("TARGET_ROOT")
                               .help("sets a custom root target")
                               .takes_value(true))
                          .arg(Arg::with_name("pkgdir")
                               .long("pkgdir")
                               .help("sets location of package directory")
                               .takes_value(true))
                          .subcommand(
                              SubCommand::with_name("package")
                                      .about("creates a package archive from .pkgspec")
                                      .arg(
                                          Arg::with_name("PKGSPEC")
                                          .required(true)
                                          .help("path to .pgkspec file"))
                                    )
                            .subcommand(
                                SubCommand::with_name("installpkg")
                                        .about("install a package from a package archive")
                                        .arg(
                                            Arg::with_name("ARCHIVE")
                                            .required(true)
                                            .help("path to package archive"))
                                        )
                            .subcommand(
                                SubCommand::with_name("install")
                                        .about("install a package from the database")
                                        .arg(
                                            Arg::with_name("NAME")
                                            .required(true)
                                            .help("package name"))
                                        )
                            .subcommand(
                                SubCommand::with_name("deps")
                                        .about("get dependencies")
                                        .arg(
                                            Arg::with_name("NAMED")
                                            .required(true)
                                            .help("package name"))
                                        )
                          .get_matches();

    let pkg_dir  = matches.value_of("pkgdir").unwrap_or(PKG_DIR);  
    let root = matches.value_of("root").unwrap_or("/");      
    init_root(root);                 
    if let Some(matches) = matches.subcommand_matches("package") {
       let pkgspec_path = matches.value_of("PKGSPEC").unwrap();
       let mut child = Command::new("mkpkg")
            .arg(pkgspec_path)
            .spawn()
            .expect("Failed to execute mkpkg. Is mkpkg installed?");

        let status = child.wait().unwrap();

        if !status.success() {
            println!("Error while executing mkpkg.");
        }

    } else if let Some(matches) = matches.subcommand_matches("installpkg") {
        let archive_path = matches.value_of("ARCHIVE").unwrap();
        install_archive(archive_path, root, pkg_dir);
        

    } else if let Some(matches) = matches.subcommand_matches("install") {
        let package_name = matches.value_of("NAME").unwrap();
        install_pkgname(package_name, root, pkg_dir);
        
    } else if let Some(matches) = matches.subcommand_matches("deps") {
        let package_name = matches.value_of("NAMED").unwrap();
        let dependencies = resolve_dependencies(package_name, pkg_dir, root);
        for d in dependencies {
            println!("{}",d);
        }
        
    }
}

fn install_archive_no_deps(archive_path: &str, root: &str) {
    let archive_path = fs::canonicalize(&PathBuf::from(archive_path)).expect("Invalid path.");
    let mut child = Command::new("mkpkg")
        .arg("-r")
        .arg(root)
        .arg("-i")
        .arg(archive_path.to_str().unwrap())
        .spawn()
        .expect("Failed to execute mkpkg. Is mkpkg installed?");

    let status = child.wait().unwrap();

    if !status.success() {
        panic!("Error while extracting archive.");
    } else {
        let name = archive_path.file_name().unwrap().to_str().unwrap().replace("tar.gz", "");
        update_installed(&name, root);
    }
}

fn install_archive(archive_path: &str, root: &str, pkg_dir: &str) {
    let package_name = get_basename(archive_path);
    let dependencies = resolve_dependencies(&package_name, pkg_dir, root);

    for d in dependencies {
        if is_installed(&d, root) {
            println!("{} is already installed. Skipping...", d);
        } else {
            let archive_path = get_latest_archive(&d, pkg_dir);
            println!("Installing {}...", d);
            install_archive_no_deps(&archive_path, root);
        }
        
    }
    if is_installed(&archive_path, root) {
        println!("Reinstalling {}...", get_basename(&archive_path));
    } else {
        println!("Installing {}...", get_basename(&archive_path));
    }
    install_archive_no_deps(&archive_path, root);
}


fn get_latest_archive(package_name: &str, pkg_dirs: &str) -> String {
    let mut curr_filename = String::new();
    let mut curr_version: i64 = -1;
    let package_name = &get_basename(package_name);

    let pkg_dir = Path::new(pkg_dirs);
    for d in fs::read_dir(pkg_dir).unwrap() {
        let filename = String::from(d.unwrap().file_name().to_str().unwrap());
        if filename.starts_with(package_name) & filename.ends_with("tar.gz") {
            let version_res = filename.replace(".tar.gz", "")
                                .replace(package_name, "")
                                .replace("-","")
                                .replace(".","")
                                .parse::<i64>();

            if !version_res.is_err() {
                let version = version_res.unwrap();
                if version >= curr_version {
                    curr_filename = filename;
                    curr_version = version;
                }
            }      
            
            
        }
    }

    if curr_filename == "" {
        panic!("Package {} not found", package_name);
    }
    let archive_path = format!("{}/{}",pkg_dirs,curr_filename);
    return archive_path
}

fn install_pkgname(package_name: &str, root: &str, pkg_dir: &str) {
    let archive_path = get_latest_archive(package_name, pkg_dir);
    install_archive(&archive_path, root, pkg_dir);
}

fn update_installed(name: &str, root: &str) {
    let basename = get_basename(name);

    let file = fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(add_prefix(INSTALLED_PKG,root)).unwrap();

    let mut file = BufReader::new(file);
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let mut is_installed = false;
    let mut target_line = String::new();

    for l in contents.lines() {
        if l.starts_with(&basename) {
            is_installed = true;
            target_line = l.replace("\n", "");
            break;
        }
    }

    if is_installed {
        contents = contents.replace(&target_line, &name);
    } else {
        contents = format!("{}\n{}", contents, get_filename(name));
    }

    let mut file = fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(false)
        .open(add_prefix(INSTALLED_PKG,root)).unwrap();
    file.write_all(contents.as_bytes()).unwrap();
}

fn is_installed(name: &str, root: &str) -> bool {
    let basename = get_basename(name);

    let file = fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(add_prefix(INSTALLED_PKG,root)).unwrap();

    let mut file = BufReader::new(file);
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let mut is_installed = false;

    for l in contents.lines() {
        if is_same_package(l, &basename) {
            is_installed = true;
            break;
        }
    }

    is_installed
}

fn get_basename(name: &str) -> String {

    let name = PathBuf::from(name).file_name().unwrap().to_str().unwrap().replace(".tar.gz", "");
    
    let mut elems: Vec<&str> = name.split("-").collect();
    if elems.len() > 1 {
        let version = elems.pop().unwrap();
        if !is_version(&version) {
            elems.push(version);
        }
    }
    let basename = elems.join("-");
    basename
}

struct PkgInfo {
    pub name: String,
    pub version: String,
    pub dependencies: Vec<String>
}

impl PkgInfo {
    fn read_pkg(filename: &str) -> Self {
        let filename = fs::canonicalize(&PathBuf::from(filename)).expect("Invalid path.");
        let filename = filename.to_str().unwrap();
        let mut child = Command::new("tar")
            .arg("-xf")
            .current_dir("/tmp")
            .arg(filename)
            .arg(format!("./tmp/{}.pkgspec",get_filename(filename)))
            .spawn()
            .expect("Failed to execute mkpkg. Is mkpkg installed?");

        let status = child.wait().unwrap();

        if !status.success() {
            panic!("Error while extracting archive.");
        }

        let mut file = fs::File::open(format!("/tmp/tmp/{}.pkgspec",get_filename(filename))).unwrap();
        let mut content = String::new();
        file.read_to_string(&mut content).unwrap();
        
        let re = Regex::new("PKG_NAME=\"(\\S+)\"").unwrap();
        let name = re.captures(&content).unwrap().get(1).unwrap().as_str();

        let re = Regex::new("PKG_VERSION=\"(\\S+)\"").unwrap();
        let version = re.captures(&content).unwrap().get(1).unwrap().as_str();

        let re = Regex::new("DEPENDS=\"(.+)\"").unwrap();
        let dependencies: Vec<String> = if let Some(capture) = re.captures(&content) {
            capture.get(1).unwrap().as_str().split(" ").map(|e| {String::from(e)}).collect()
        } else {
            Vec::new()
        };

        Self {
            name: String::from(name),
            version: String::from(version),
            dependencies: dependencies
        }
    }
}

fn resolve_dependencies(package_name: &str, pkg_dir: &str, root: &str) -> Vec<String> {
    let mut deps_set = HashSet::new();
    let mut deps = Vec::new();
    resolve_dependencies_recurse(&package_name, &mut deps_set, pkg_dir, &mut deps, root);
    deps


}

fn resolve_dependencies_recurse(package_name: &str, deps_acc_set: &mut HashSet<String>, pkg_dir: &str, deps_acc: &mut Vec<String>, root: &str) {
    let archive_path = get_latest_archive(package_name, pkg_dir);
    let pkginfo = PkgInfo::read_pkg(&archive_path);

    let mut deps = pkginfo.dependencies;

    if (pkginfo.name != "base") & (pkginfo.name != "filesystem") {
        deps.push(String::from("base"));
        deps.push(String::from("filesystem"));
    }

    for d in deps {
        if !deps_acc_set.contains(&d) & !is_installed(&d, root) {
            deps_acc_set.insert(d.clone());
            resolve_dependencies_recurse(&d, deps_acc_set, pkg_dir, deps_acc, root);
            deps_acc.push(d.clone());
            
        }
    }
}

fn get_filename(path: &str) -> String {
    let name = PathBuf::from(path).file_name().unwrap().to_str().unwrap().replace(".tar.gz", "");
    String::from(name)
} 

fn add_prefix(path: &str, prefix: &str) -> String {
    let ret = format!("{}/{}", prefix.trim_end_matches("/"),path.trim_start_matches("/"));
    ret
}

fn is_same_package(name: &str, basename: &str) -> bool  {
    let name = name.trim().trim_end_matches(".");
    let basename = basename.trim().trim_end_matches(".");
    name.replace(".tar.gz", "")
                                .replace(basename, "")
                                .replace("-","")
                                .replace(".","")
                                .parse::<i64>().is_ok()
}

fn is_version(name: &str) -> bool {
    name.replace(".","").parse::<i64>().is_ok()
}

fn init_root(root: &str) {
    let create_root = fs::OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(add_prefix(INSTALLED_PKG,root)).is_err();

    if create_root {
        Command::new("mkdir")
        .arg("-p")
        .arg(add_prefix("/var/newt",root))
        .spawn()
        .expect("Failed to init root").wait().unwrap();
    }
} 